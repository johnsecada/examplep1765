This repository contains an example of sharing files among many users.

The code is related to the IEEE P1765 Estimating the Uncertainty In Measurements of Modulated Signals. For further details see 

https://standards.ieee.org/develop/project/1765.html

This open source repository contains material that may be included-in
or referenced by an unapproved draft of a proposed IEEE Standard. All
material in this repository is subject to change. The material in this
repository is presented "as is" and with all faults. Use of the
material is at the sole risk of the user. IEEE specifically disclaims
all warranties and representations with respect to all material
contained in this repository and shall not be liable, under any
theory, for any use of the material. Unapproved drafts of proposed
IEEE standards must not be utilized for any conformance/compliance
purposes. See the NOTICE.md file distributed with this work for
copyright and licensing information.

