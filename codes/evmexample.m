hMod = comm.RectangularQAMModulator(16); 
hAWGN = comm.AWGNChannel('NoiseMethod',...
        'Signal to noise ratio (SNR)',...
        'SNR', 20, 'SignalPower', 10); 

% Create an EVM object, output maximum and 90-percentile EVM
% measurements, and symbol count
    hEVM = comm.EVM('MaximumEVMOutputPort',true,...
            'XPercentileEVMOutputPort', true, 'XPercentileValue', 90,...
            'SymbolCountOutputPort', true);    
 % Generate modulated symbols and add noise
    refsym = step(hMod, randi([0 15], 1000, 1));
    rxsym = step(hAWGN, refsym);
 % Calculate measurements
    [RMSEVM,MaxEVM,PercentileEVM,NumSym] = step(hEVM,refsym,rxsym)